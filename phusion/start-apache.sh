#!/bin/sh

APACHE_RUN_DIR=/var/run/apache2

# Clear PID-files
if [ "$(ls -A $APACHE_RUN_DIR)" ]; then
    rm -f $APACHE_RUN_DIR/*
fi

# Start apache
/usr/sbin/apache2ctl -D FOREGROUND
