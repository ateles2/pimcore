#!/bin/sh

MEMORY_LIMIT=512M
POST_MAXSIZE=200M
UPLOAD_MAX_FILESIZE=200M
DATE_TIMEZONE=Europe/Stockholm

PHP_VERSION=7.1

sed -i "/memory_limit =/c\memory_limit = $MEMORY_LIMIT" /etc/php/$PHP_VERSION/apache2/php.ini
sed -i "/memory_limit =/c\memory_limit = $MEMORY_LIMIT" /etc/php/$PHP_VERSION/cli/php.ini
sed -i "/post_max_size =/c\post_max_size = $POST_MAXSIZE" /etc/php/$PHP_VERSION/apache2/php.ini
sed -i "/post_max_size =/c\post_max_size = $POST_MAXSIZE" /etc/php/$PHP_VERSION/cli/php.ini
sed -i "/upload_max_filesize =/c\upload_max_filesize = $UPLOAD_MAX_FILESIZE" /etc/php/$PHP_VERSION/apache2/php.ini
sed -i "/upload_max_filesize =/c\upload_max_filesize = $UPLOAD_MAX_FILESIZE" /etc/php/$PHP_VERSION/cli/php.ini

# date timezone
sed -i "/;date.timezone =/c\date.timezone = $DATE_TIMEZONE" /etc/php/$PHP_VERSION/apache2/php.ini
sed -i "/;date.timezone =/c\date.timezone = $DATE_TIMEZONE" /etc/php/$PHP_VERSION/cli/php.ini
